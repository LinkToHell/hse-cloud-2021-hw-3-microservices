import os
import json
import asyncio
import aiohttp
import pandas as pd


def stupid_count_tokens(tokens, text):
    res = 0
    for token in tokens:
        if token in text:
            res += 1
    return res


class SimpleSearchService:
    def __init__(self):
        self._data = None
        self.DOCS_COLUMNS = ['document', 'key', 'key_md5']

    def _build_tokens_count(self, search_text):
        tokens = search_text.split()
        res = self._data['document'].apply(lambda x: stupid_count_tokens(tokens, x))
        res.name = None
        return res

    def _get_gender_mask(self, user_data=None):
        ud = user_data.get('gender', 'null') if user_data is not None else 'non-existing gender'
        return self._data['gender'].apply(lambda x: stupid_count_tokens([ud], x))

    def _get_age_mask(self, user_data=None):
        user_age = int(user_data['age']) if user_data is not None else -1
        return self._data.apply(lambda x: x['age_from'] <= user_age <= x['age_to'], axis=1)

    def _sort_by_rating_and_tokens(self, rating, tokens_count, key_md5):
        df = pd.concat([tokens_count, rating, key_md5], axis=1)
        return df.sort_values([0, 1, 'key_md5'], ascending=[False, False, False])

    def get_search_data(self, search_text, user_data=None, limit=10) -> pd.DataFrame:
        if search_text is None or search_text == '':
            return pd.DataFrame([], columns=self.DOCS_COLUMNS)
        tokens_count = self._build_tokens_count(search_text)
        gender_mask = self._get_gender_mask(user_data)
        age_mask = self._get_age_mask(user_data)
        rating = gender_mask + age_mask
        df = self._sort_by_rating_and_tokens(rating, tokens_count, self._data['key_md5'])
        return self._data.loc[df.head(limit).index]


class SearchInShardsService(SimpleSearchService):
    def __init__(self):
        super(SearchInShardsService, self).__init__()
        simple_search_service_hosts = os.environ['SIMPLE_SEARCH_SERVICE_HOSTS'].split(',')
        self._simple_search_service_paths = [f'{host}/search' for host in simple_search_service_hosts]

    async def _get_url(self, url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url=url) as response:
                return pd.DataFrame(await response.json(content_type=None))

    async def _get_urls(self, urls):
        return await asyncio.gather(*[self._get_url(url) for url in urls])

    def get_search_data(self, search_text, user_data=None, limit=10) -> str:
        simple_search_service_qs = f'text={search_text}&user_data={user_data}&limit={limit}'
        simple_search_service_urls = [f'http://{path}?{simple_search_service_qs}' for path in self._simple_search_service_paths]
        shards_responses = asyncio.run(self._get_urls(simple_search_service_urls))
        self._data = pd.concat(shards_responses)
        self._data.reset_index(inplace=True, drop=True)
        search_data = super().get_search_data(search_text, json.loads(user_data), int(limit))
        return json.dumps(search_data[self.DOCS_COLUMNS].to_dict('records'))
