import os
import pandas as pd
from flask import Flask, request


class UserService:
    key = 'user_id'
    data_keys = ('gender', 'age')

    def __init__(self):
        self._data = dict()
        data_path = os.environ['DATA_PATH']
        data = pd.read_csv(data_path).to_dict('records')
        for row in data:
            assert row[self.key] not in self._data, f'Key value {self.key}={row[self.key]} is not unique in self._data'
            self._data[row[self.key]] = {k: row[k] for k in self.data_keys}

    def get_user_data(self, user_id):
        return self._data.get(user_id)


class Server(Flask):
    def __init__(self, name: str, service: UserService):
        super().__init__(name)
        self._service = service
        self.add_url_rule('/search', 'search', self.search)
        self._port = os.environ['HTTP_FLASK_PORT']

    def search(self):
        user_id = int(request.args.get('user_id'))
        return self._service.get_user_data(user_id)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=self._port, **kwargs)


def main():
    user_service = UserService()
    server = Server('user', service=user_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
