import os
import json
from flask import Flask, request
from service import SimpleSearchService


class Server(Flask):
    def __init__(self, name: str, service: SimpleSearchService):
        super().__init__(name)
        self._service = service
        self.add_url_rule('/search', 'search', self.search)
        self._port = os.environ['HTTP_FLASK_PORT']

    def search(self):
        text = request.args.get('text')
        limit = int(request.args.get('limit'))
        user_data = json.loads(request.args.get('user_data'))
        return self._service.get_search_data(text, user_data, limit)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=self._port, **kwargs)


def main():
    simple_search_service = SimpleSearchService()
    server = Server('simple_search', service=simple_search_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
