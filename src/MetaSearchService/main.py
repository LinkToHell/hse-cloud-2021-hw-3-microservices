import os
import requests
from flask import Flask, request


class MetaSearchService:
    def __init__(self) -> None:
        user_service_host = os.environ['USER_SERVICE_HOST']
        search_in_shards_service_host = os.environ['SEARCH_IN_SHARDS_SERVICE_HOST']

        self._user_service_path = f'{user_service_host}/search'
        self._search_in_shards_service_path = f'{search_in_shards_service_host}/search'

    def search(self, search_text, user_id, limit=10) -> str:
        user_service_qs = f'user_id={user_id}'
        user_service_url = f'http://{self._user_service_path}?{user_service_qs}'
        user_data = requests.get(user_service_url).text

        search_in_shards_service_qs = f'text={search_text}&user_data={user_data}&limit={limit}'
        search_in_shards_service_url = f'http://{self._search_in_shards_service_path}?{search_in_shards_service_qs}'
        search_data = requests.get(search_in_shards_service_url).json()

        return search_data


class Server(Flask):
    def __init__(self, name: str, service: MetaSearchService):
        super().__init__(name)
        self._service = service
        self.add_url_rule('/search', 'search', self.search)
        self._port = os.environ['HTTP_FLASK_PORT']

    def search(self):
        text = request.args.get('text')
        user_id = request.args.get('user_id')
        search_results = self._service.search(text, user_id)
        return {'search_results': search_results}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=self._port, **kwargs)


def main():
    meta_search_service = MetaSearchService()
    server = Server('meta_search', service=meta_search_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
